package fr.adrienrogliano.Note;

import fr.adrienrogliano.Note.Models.Filiere;
import fr.adrienrogliano.Note.Models.Matiere;
import fr.adrienrogliano.Note.Models.Note;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class NoteService {

    @Autowired
    CrudRepositoryFiliere crudRepositoryFiliere;

    @GetMapping("/filieres")
    public Iterable<Filiere> getAllFilieres() {
        return crudRepositoryFiliere.findAll();
    }

    @GetMapping("/filieres/{id}")
    public Filiere getFiliereById(@PathVariable(value = "id") Long id) {
        return crudRepositoryFiliere.findById(id).orElse(null);
    }

    @GetMapping("/matieres")
    public Map<Long, String> getAllMatieres() {
        Map<Long, String> map = new HashMap<>();
        for (Filiere f : crudRepositoryFiliere.findAll()) {
            for (Matiere m : f.getMatieres()) {
                map.put(m.getId(), m.getNom());
            }
        }
        return map;
    }

    @GetMapping("/matieres/{id}")
    public Matiere getMatiereById(@PathVariable(value = "id") Long id) {
        for (Filiere f : crudRepositoryFiliere.findAll()) {
            for(Matiere m : f.getMatieres()) {
                if (id.equals(m.getId())) {
                    return m;
                }
            }
        }
        return null;
    }


    @GetMapping("/notes")
    public Map<Long, String> getAllNotes() {
        Map<Long, String> map = new HashMap<>();
        for (Filiere f : crudRepositoryFiliere.findAll()) {
            for (Matiere m : f.getMatieres()) {
                for (Note n : m.getNotes()) {
                    map.put(n.getId(), n.getAppartient());
                }
            }
        }

        return map;
    }

    @GetMapping("/notes/{id}")
    public Note getNotesById(@PathVariable(value = "id") Long id) {
        for (Filiere f : crudRepositoryFiliere.findAll()) {
            for (Matiere m : f.getMatieres()) {
                for (Note n : m.getNotes()) {
                    if (id.equals(n.getId())){
                        return n;
                    }
                }
            }
        }

        return null;
    }

    @PutMapping("/filieres/add")
    public Filiere addFiliere(@RequestBody Filiere f) {
        crudRepositoryFiliere.save(f);
        System.err.println(crudRepositoryFiliere.findById(f.getId()).orElse(null).getId());
        return crudRepositoryFiliere.findById(f.getId()).orElse(null);
    }

    @PutMapping("/matieres/add")
    public Filiere addMatiere(@RequestBody Matiere m) throws Exception {
        Optional<Filiere> optional = crudRepositoryFiliere.findById(m.getFID());
        if (optional.isPresent()) {
            Filiere f = optional.get();
            f.getMatieres().add(m);
            m.setFiliere(f);
            crudRepositoryFiliere.save(f);
        }
        else {
            throw new Exception("Filière id non valide !");
        }
        return crudRepositoryFiliere.findById(m.getFID()).orElse(null);
    }



    @PutMapping("/notes/add")
    public Filiere addNote(@RequestBody Note n) throws Exception {
        Optional<Filiere> optional = crudRepositoryFiliere.findById(n.getFID());
        if (optional.isPresent()) {
            Filiere f = optional.get();
            for (Matiere matiere : f.getMatieres()) {
                if (n.getMID().equals(matiere.getId())) {
                    n.setMatiere(matiere);
                    matiere.getNotes().add(n);
                    crudRepositoryFiliere.save(f);
                }
            }
        }
        else {
            throw new Exception("Filière id non valide !");
        }

        return crudRepositoryFiliere.findById(n.getFID()).orElse(null);
    }



    @DeleteMapping("/filieres/delete/{id}")
    public String deleteFiliere(@PathVariable(value = "id") Long id) {
        crudRepositoryFiliere.deleteById(id);
        return "Deleted";
    }

    @DeleteMapping("/matieres/delete/{id}")
    public String deleteMatieres(@PathVariable(value = "id") Long id) {
        Matiere m = getMatiereById(id);
        if (m == null) {
            return "Matière not found";
        }

        Filiere f = getFiliereById(m.getFID());
        f.getMatieres().remove(m);
        m.setFiliere(null);
        crudRepositoryFiliere.save(f);
        return "Deleted";

    }
    @DeleteMapping("/notes/delete/{id}")
    public String deleteNotes(@PathVariable(value = "id") Long id) {
        Note n = getNotesById(id);
        if (n == null) {
            return "Notes not found";
        }

        Filiere f = getFiliereById(n.getFID());
        for (Matiere m : f.getMatieres()) {
            if (m.getId() == n.getMID()) {
                m.getNotes().remove(n);
                n.setMatiere(null);
            }
        }

        crudRepositoryFiliere.save(f);
        return "Deleted";
    }
}
