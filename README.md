#   Projet M1
Vous trouverez ici le code des micro services ainsi que les fichiers de configurations de **Kubernetes**.  
Le code du Front-end fait à l'aide d'Angular JS se trouve dans un repository sur mon gitlab, Ce [**lien**](https://gitlab.com/ad92i/projet-angular) vous y redirige.

#   Deployer le site web

Les tests de ces fichiers de configurations ont été fait avec k3s.

On démarre le service k3s avec systemd.
```
sudo systemctl start k3s
```

```bash
cd Kubernetes
chmod +x ./deploy.sh
./deploy.sh
```

Une fois l'éxécution en cours le script va attendre 15s afin de laisser le temps à tous les clusters de démarrer.  
Le script va après indiqué l'adresse IP du front web service.

#   Annuler le deploiment

```bash
cd Kubernetes
chmod +x delete.sh
./delete.sh
```
