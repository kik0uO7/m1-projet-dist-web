package fr.adrienrogliano.Gateway.controller.security;

import fr.adrienrogliano.Gateway.controller.repository.UserRepository;
import fr.adrienrogliano.Gateway.model.MongoUserDetails;
import fr.adrienrogliano.Gateway.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user == null || user.getRole() == null || user.getRole().isEmpty()) {
            throw new UsernameNotFoundException("Invalid username or password. "  + HttpStatus.UNAUTHORIZED);
        }

        String [] authorities = new String[1];

        authorities[0] = user.getRole();
        MongoUserDetails userDetails = new MongoUserDetails(user.getEmail(),user.getPassword(),user.getActive(),
                user.isLocked(), user.isExpired(),user.isEnabled(),authorities);
        return userDetails;
    }

}