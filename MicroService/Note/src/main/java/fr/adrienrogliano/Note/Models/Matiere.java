package fr.adrienrogliano.Note.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator(name="matieres", initialValue=200001)
public class Matiere {

    public enum NOTATION {
        PROJET, MAX_EXAM, MOYENNE
    }

    // autogenerer
    private Long id;
    private String nom;
    private int ects;
    private int nbNotes;
    private List<Note> notes = new ArrayList<>();
    private NOTATION typeNotation;
    private Long FID;
    private Filiere filiere;
    private boolean option;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="matieres")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNbNotes() {
        return nbNotes;
    }
    public void setNbNotes(int nbNotes) {
        this.nbNotes = nbNotes;
    }

    public int getEcts() {
        return ects;
    }

    public void setEcts(int ects) {
        this.ects = ects;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matiere")
    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public NOTATION getTypeNotation() {
        return typeNotation;
    }
    public void setTypeNotation(NOTATION typeNotation) {
        this.typeNotation = typeNotation;
    }

    public Long getFID() {
        return FID;
    }

    public void setFID(Long FID) {
        this.FID = FID;
    }

    @ManyToOne
    @JsonIgnore
    public Filiere getFiliere() {
        return filiere;
    }

    public void setFiliere(Filiere filiere) {
        this.filiere = filiere;
    }

    public boolean isOption() {
        return option;
    }

    public void setOption(boolean option) {
        this.option = option;
    }

    @Override
    public String toString() {
        return "Matiere{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", ects=" + ects +
                ", notes=" + notes +
                ", typeNotation=" + typeNotation +
                ", filiere_id=" + FID +
                ", filiere=" + filiere.getNom() +
                ", option=" + option +
                '}';
    }
}
