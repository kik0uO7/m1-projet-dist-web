package fr.adrienrogliano.Note.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@SequenceGenerator(name="notes", initialValue=300001)
public class Note {

    private Long id;
    private double valeur;
    private double coefficient;
    private String appartient;
    private Matiere matiere;
    private Long MID;
    private Long FID;
    private boolean exam;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="notes")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getValeur() {
        return valeur;
    }

    public void setValeur(double valeur) {
        this.valeur = valeur;
    }

    public double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    public String getAppartient() {
        return appartient;
    }

    public void setAppartient(String appartient) {
        this.appartient = appartient;
    }

    @ManyToOne
    @JsonIgnore
    public Matiere getMatiere() {
        return matiere;
    }

    public void setMatiere(Matiere matiere) {
        this.matiere = matiere;
    }

    public Long getMID() {
        return MID;
    }

    public void setMID(Long MID) {
        this.MID = MID;
    }

    public Long getFID() {
        return FID;
    }

    public void setFID(Long FID) {
        this.FID = FID;
    }

    public boolean isExam() {
        return exam;
    }

    public void setExam(boolean exam) {
        this.exam = exam;
    }

    @Override
    public String toString() {
        return "Note{" +
                "id=" + id +
                ", valeur=" + valeur +
                ", coefficient=" + coefficient +
                ", appartient='" + appartient + '\'' +
                ", matiere=" + matiere.getNom() +
                ", MID=" + MID +
                ", FID=" + FID +
                ", exam=" + exam +
                '}';
    }


}
