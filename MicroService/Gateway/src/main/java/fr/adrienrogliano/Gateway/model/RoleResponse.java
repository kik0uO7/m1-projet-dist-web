package fr.adrienrogliano.Gateway.model;

public class RoleResponse {
    private String roleName;

    public RoleResponse(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
