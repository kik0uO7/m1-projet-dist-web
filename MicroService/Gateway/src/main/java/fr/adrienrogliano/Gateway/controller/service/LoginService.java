package fr.adrienrogliano.Gateway.controller.service;

import fr.adrienrogliano.Gateway.controller.repository.JwtTokenRepository;
import fr.adrienrogliano.Gateway.controller.repository.UserRepository;
import fr.adrienrogliano.Gateway.controller.security.JwtTokenUtil;
import fr.adrienrogliano.Gateway.model.JwtToken;
import fr.adrienrogliano.Gateway.model.User;
import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;
import java.util.Optional;

@Service
public class LoginService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtTokenRepository jwtTokenRepository;


    public User userByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public Iterable<User> allUsers() {
        return userRepository.findAll();
    }

    public Iterable<JwtToken> allTokens() {
        for (JwtToken j : jwtTokenRepository.findAll()) {
            if (!isValidToken(j.getToken())) {
                jwtTokenRepository.delete(j);
            }
        }
        return jwtTokenRepository.findAll();
    }

    public String login(String username, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        User user = userRepository.findByEmail(username);
        if (user == null || user.getRole() == null || user.getRole().isEmpty()) {
            throw new JwtException("Invalid username or password." + HttpStatus.UNAUTHORIZED);
        }

        return jwtTokenUtil.createToken(username, user.getRole());

    }

    public User saveUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    public boolean logout(String token) {
        jwtTokenRepository.delete(new JwtToken(token));
        return true;
    }

    public boolean isValidToken(String token) {
        boolean valid;
        try {
            valid = jwtTokenUtil.validateToken(token);
        }
        catch (JwtException e) {
            valid = false;
        }
        return valid;
    }

    public String createNewToken(String token) {
        String username = jwtTokenUtil.getUsername(token);
        String roleName = jwtTokenUtil.getRoleList(token);

        this.jwtTokenRepository.delete(new JwtToken(token));

        return jwtTokenUtil.createToken(username, roleName);
    }

}