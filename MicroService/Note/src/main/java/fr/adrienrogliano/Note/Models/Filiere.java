package fr.adrienrogliano.Note.Models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator(name="filieres", initialValue=1)
public class Filiere {
    private Long id;
    private String nom;
    private List<Matiere> matieres = new ArrayList<>();

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="filieres")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "filiere")
    public List<Matiere> getMatieres() {
        return matieres;
    }

    public void setMatieres(List<Matiere> matieres) {
        this.matieres = matieres;
    }

    @Override
    public String toString() {
        return "Filiere{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", matieres=" + matieres +
                '}';
    }

}
