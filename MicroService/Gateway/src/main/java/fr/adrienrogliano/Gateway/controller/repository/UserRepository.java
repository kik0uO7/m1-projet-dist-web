package fr.adrienrogliano.Gateway.controller.repository;

import fr.adrienrogliano.Gateway.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {
    User findByEmail(String email);
}
