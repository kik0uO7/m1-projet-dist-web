package fr.adrienrogliano.Note;

import fr.adrienrogliano.Note.Models.Filiere;
import org.springframework.data.repository.CrudRepository;

public interface CrudRepositoryFiliere extends CrudRepository<Filiere, Long> { }
