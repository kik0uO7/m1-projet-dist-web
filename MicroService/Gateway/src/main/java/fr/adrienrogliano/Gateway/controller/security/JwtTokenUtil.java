package fr.adrienrogliano.Gateway.controller.security;

import fr.adrienrogliano.Gateway.controller.repository.JwtTokenRepository;
import fr.adrienrogliano.Gateway.model.JwtToken;
import fr.adrienrogliano.Gateway.model.MongoUserDetails;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Date;

@Component
public class JwtTokenUtil implements Serializable {
    private static final long ACCESS_TOKEN_VALIDITY_SECONDS = 3600;

    @Value("${jwt.security.key}")
    private String jwtKey;

    @Autowired
    private JwtTokenRepository jwtTokenRepository;

    public String createToken(String username, String roles) {
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("auth", roles);


        String s =  Jwts.builder()
                .setClaims(claims)
                .setIssuer("adrien-rogliano")
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + ACCESS_TOKEN_VALIDITY_SECONDS*1000))
                .signWith(SignatureAlgorithm.HS512, jwtKey)
                .compact();

        jwtTokenRepository.save(new JwtToken(s));

        return s;
    }

    public String resolveToken(HttpServletRequest req) {
        String token = req.getHeader("Authorization");
        if (token != null) {
            token = token.replace("Bearer ", "");
            return token;
        }
        return null;
    }

    public boolean validateToken(String token) throws JwtException, IllegalArgumentException, ExpiredJwtException {
        Jwts.parser().setSigningKey(jwtKey).parseClaimsJws(token);
        return true;
    }

    public boolean isTokenPresentInDB (String token) {
        return jwtTokenRepository.findById(token).isPresent();
    }


    public String getRoleList(String token) {
        return (String) Jwts.parser().setSigningKey(jwtKey).parseClaimsJws(token).
                getBody().get("auth");
    }


    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(jwtKey).parseClaimsJws(token).getBody().getSubject();
    }


    public UserDetails getUserDetails(String token) {
        String userName =  getUsername(token);
        String roleName = getRoleList(token);
        UserDetails userDetails = new MongoUserDetails(userName,roleName);
        return userDetails;
    }

    public Authentication getAuthentication(String token) {

        //UserDetails userDetails = userDetailsService.loadUserByUsername(getUsername(token));

        UserDetails userDetails = getUserDetails(token);
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

}