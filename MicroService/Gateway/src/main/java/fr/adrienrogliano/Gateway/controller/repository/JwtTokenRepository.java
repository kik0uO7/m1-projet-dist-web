package fr.adrienrogliano.Gateway.controller.repository;

import fr.adrienrogliano.Gateway.model.JwtToken;
import org.springframework.data.repository.CrudRepository;

public interface JwtTokenRepository extends CrudRepository<JwtToken, String> {
}
