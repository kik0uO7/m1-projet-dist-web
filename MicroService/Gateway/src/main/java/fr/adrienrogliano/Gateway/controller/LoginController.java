package fr.adrienrogliano.Gateway.controller;

import fr.adrienrogliano.Gateway.controller.service.LoginService;
import fr.adrienrogliano.Gateway.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class LoginController {
    @Autowired
    DiscoveryClient discoveryClient;

    @Autowired
    private LoginService loginService;

    @GetMapping("/users")
    public Iterable<User> getAllUsers() {
        return loginService.allUsers();
    }

    @GetMapping("/tokens")
    public Iterable<JwtToken> getAllTokens() {
        return loginService.allTokens();
    }

    @PostMapping("/login/role")
    public ResponseEntity<RoleResponse> getUsersByEmail(@RequestBody UserEmail email) {
        User user = loginService.userByEmail(email.getEmail());
        return new ResponseEntity<>(new RoleResponse(user.getRole()), HttpStatus.ACCEPTED);

    }

    @PostMapping("/login")
    @ResponseBody
    public ResponseEntity<AuthResponse> login(@RequestBody LoginRequest loginRequest) {
        String token = loginService.login(loginRequest.getUsername(),loginRequest.getPassword());
        HttpHeaders headers = new HttpHeaders();
        List<String> headerList = new ArrayList<>();
        List<String> exposeList = new ArrayList<>();
        headerList.add("Content-Type");
        headerList.add(" Accept");
        headerList.add("X-Requested-With");
        headerList.add("Authorization");
        headers.setAccessControlAllowHeaders(headerList);
        exposeList.add("Authorization");
        headers.setAccessControlExposeHeaders(exposeList);
        headers.set("Authorization", token);
        return new ResponseEntity<AuthResponse>(new AuthResponse(token), headers, HttpStatus.CREATED);
    }

    @PostMapping("/logout")
    @ResponseBody
    public ResponseEntity<AuthResponse> logout (@RequestHeader(value="Authorization") String token) {
        HttpHeaders headers = new HttpHeaders();
        if (loginService.logout(token)) {
            headers.remove("Authorization");
            return new ResponseEntity<>(new AuthResponse("logged out"), headers, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(new AuthResponse("Logout Failed"), headers, HttpStatus.NOT_MODIFIED);
    }

    @PostMapping("/register")
    @ResponseBody
    public User createUser(@RequestBody RegisterUserRequest registerBean) {
        User user = new User();
        user.setEmail(registerBean.getEmail());
        user.setFirstName(registerBean.getFirstName());
        user.setLastName(registerBean.getLastName());
        user.setPassword(registerBean.getPassword());
        user.setRole("ROLE_"+registerBean.getRoleName());

        return loginService.saveUser(user);

    }
}