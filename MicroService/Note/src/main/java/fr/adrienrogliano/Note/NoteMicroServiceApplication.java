package fr.adrienrogliano.Note;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NoteMicroServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(NoteMicroServiceApplication.class, args);
	}
}
