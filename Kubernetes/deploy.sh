#!/bin/bash

# Claims + Volumes
sudo kubectl apply -f mongo/mongo-volume.yml
sudo kubectl apply -f mongo/mongo-claim.yml
sudo kubectl apply -f note/note-volume.yml
sudo kubectl apply -f note/note-claim.yml

# Deployments
sudo kubectl apply -f registry-app.yml
sudo kubectl apply -f gateway-app.yml
sudo kubectl apply -f front-app.yml
sudo kubectl apply -f note/note-app_pv.yml
sudo kubectl apply -f mongo/mongo-app_pv.yml

# Services
sudo kubectl apply -f registry-app-service.yml
sudo kubectl apply -f gateway-app-service.yml
sudo kubectl apply -f front-app-service.yml
sudo kubectl apply -f note/note-app-service.yml
sudo kubectl apply -f mongo/mongo-app-service.yml

# Ingress 
sudo kubectl apply -f front-to-back-ingress.yml



echo "Please wait"
sleep 35
echo "Access to website with http://"`sudo kubectl get service front-service | awk '{print $3}' | tail -1`"/"

# Open in browser the front website.
 xdg-open 'http://'`sudo kubectl get service front-service | awk '{print $3}' | tail -1`

# Open in browser the h2 console.
#xdg-open 'http://'`sudo kubectl get service note-service | awk '{print $3}' | tail -1`/h2-console

