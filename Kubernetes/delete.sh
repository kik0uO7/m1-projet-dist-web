#!/bin/bash

sudo kubectl delete services --all
sudo kubectl delete deployments --all
sudo kubectl delete ingress --all
sudo kubectl delete pvc --all
sudo kubectl delete pv --all
